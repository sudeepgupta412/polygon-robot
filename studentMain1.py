from robot import *
from math import *
from matrix import *
import random

def angle_trunc(a):
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

#measurement_vector =[]
#heading = 0
#angle = []

#read_values = 0
# This is the function you have to write. The argument 'measurement' is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""
    #print measurement
    # You must return xy_estimate (x, y), and OTHER (even if it is None)     
    # in this order for grading purposes.
    #print ('OTHER =' + str(OTHER))
    #print ('measurement =' + str(measurement))
    if OTHER is None:
        OTHER = []
        angle = 0
    
    if len(OTHER) < 2:
        xy_estimate = measurement
        angle = 0
    else:
        i = len(OTHER)
        current_measurement = measurement
        prev_measurement = OTHER[i-1][0]
        prev_measurement2 = OTHER[i-2][0]
        distance1 = distance_between(prev_measurement,current_measurement)
        heading = atan2(current_measurement[1] - prev_measurement[1], current_measurement[0] - prev_measurement[0])
        heading_old = atan2(prev_measurement[1] - prev_measurement2[1], prev_measurement[0] - prev_measurement2[0])
        turn = heading - heading_old
        est_heading = angle_trunc(turn + heading)
        est_distance = distance1
        est_x = current_measurement[0] + est_distance * cos(est_heading)
        est_y = current_measurement[1] + est_distance * sin(est_heading)
        #measurement_vector.append(measurement)
        angle = est_heading
        
        xy_estimate = (est_x, est_y)
    
    OTHER.append((measurement,angle))
 
    #print ('xy_estimate =' + str(xy_estimate))
    return xy_estimate, OTHER 
    #return xy_estimate, OTHER 

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# This is here to give you a sense for how we will be running and grading
# your code. Note that the OTHER variable allows you to store any 
# information that you want. 
def slope(point1, point2):
    x1 = point1[0]
    y1 = point1[1]
    x2 = point2[0]
    y2 = point2[1]
    return float((y2-y1)/(x2-x1))

def angle(slope1, slope2):
    #print ('slope1' + str(slope1))
    #print ('slope2' + str(slope2))
    
    return ((atan2(slope2,slope1))%(2*pi))
    
    