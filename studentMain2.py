# ----------
# Part Two
#
# Now we'll make the scenario a bit more realistic. Now Traxbot's
# sensor measurements are a bit noisy (though its motions are still
# completetly noise-free and it still moves in an almost-circle).
# You'll have to write a function that takes as input the next
# noisy (x, y) sensor measurement and outputs the best guess 
# for the robot's next position.
#
# ----------
# YOUR JOB
#
# Complete the function estimate_next_pos. You will be considered 
# correct if your estimate is within 0.01 stepsizes of Traxbot's next
# true position. 
#
# ----------
# GRADING
# 
# We will make repeated calls to your estimate_next_pos function. After
# each call, we will compare your estimated position to the robot's true
# position. As soon as you are within 0.01 stepsizes of the true position,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot.

# These import steps give you access to libraries which you may (or may
# not) want to use.
from robot import *  # Check the robot.py tab to see how this works.
from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random

# This is the function you have to write. Note that measurement is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.



def filter(x, P, measurements, F, H, I, R):
    for n in range(len(measurements)):
        
        # measurement update
        z = matrix([[measurement[0]], [measurement[1]]])
        #y = Z.transpose() - (H * x)
        y = Z - (H * x)
        S = H * P * H.transpose() + R
        K = P * H.transpose() * S.inverse()
        x = x + (K * y)
        P = (I - (K * H)) * P
        
        # prediction
        x = (F * x) + u
        P = F * P * F.transpose()
        
        
    
    print 'x= '
    x.show()
    print 'P= '
    P.show()
    
    return x, P
def mode_length(line_len):
    return max(set(line_len), key=line_len.count)

def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.
    #### DO NOT MODIFY ANYTHING ABOVE HERE ####
    #### fill this in, remember to use the matrix() function!: ####
    
    #P =  # initial uncertainty: 0 for positions x and y, 1000 for the two velocities
    #F =  # next state function: generalize the 2d version to 4d
    #H =  # measurement function: reflect the fact that we observe x and y but not the two velocities
    #R =  # measurement uncertainty: use 2x2 matrix with 0.1 as main diagonal
    #I =  # 4d identity matrix
    #xy_estimate = (3.2, 9.1)   
    

    if OTHER is None:
        heading = 0
        turn = 0
        distance1 = 0
        x = matrix([[measurement[0]], [measurement[1]], [heading], [turn], [distance1]])
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        OTHER = {'steps': 0, 'EKF_status' : 0,'measurements': [], 'EKF_x': x, 'EKF_P': P, 'line_length' : [], 'line_length_calculated' : 0, 'xy_estimate' : (0,0), 'turning' : 0, 'heading': 0, 'distance' : []}
    
    OTHER['measurements'].append(measurement)
    
    if len(OTHER['measurements']) == 1:
        OTHER['steps'] = OTHER['steps'] + 1
        est_x = OTHER['measurements'][-1][0]
        est_y = OTHER['measurements'][-1][1]
        xy_estimate = OTHER['measurements'][-1][0], OTHER['measurements'][-1][1]
    
    elif len(OTHER['measurements']) == 2:
        OTHER['steps'] = OTHER['steps'] + 1
        est_x = OTHER['measurements'][-1][0]
        est_y = OTHER['measurements'][-1][1]
        xy_estimate = OTHER['measurements'][-1][0], OTHER['measurements'][-1][1]
    
    elif len(OTHER['measurements']) == 3:
        OTHER['steps'] = OTHER['steps'] + 1
        distance1 = distance_between(OTHER['measurements'][-1], OTHER['measurements'][-2])
        heading = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
        est_x = OTHER['measurements'][-1][0] + distance1 * cos(heading)
        est_y = OTHER['measurements'][-1][1] + distance1 * sin(heading)
    else:
        distance1 = distance_between(OTHER['measurements'][-1], OTHER['measurements'][-2])
        OTHER['distance'].append((distance1))
        avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
        heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
        heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
        turning = abs(heading1 - heading2)
        OTHER['heading'] = angle_trunc(turning + heading1)
        if OTHER['line_length_calculated'] == 0 :
            #print 'hi'
            
            if turning < 2*pi/14:
                est_x = OTHER['measurements'][-1][0] + distance1 * cos(heading1)
                est_y = OTHER['measurements'][-1][1] + distance1 * sin(heading1)
                OTHER['steps'] = OTHER['steps'] + 1
            
            else:
                #print 'hi'
                OTHER['turning'] = turning
                if len(OTHER['line_length']) > 0:
                    #print 'calculating line_length'
                    line_len = mode_length(OTHER['line_length'])
                    #print line_len
                else:
                    line_len = 0
                
                if line_len == OTHER['steps']:
                    #print ('line_length = ' + str(line_len))
                    OTHER['line_length_calculated'] = 1
                    
                
                OTHER['line_length'].append(OTHER['steps'])
                OTHER['steps'] = 1  
                est_x = OTHER['measurements'][-1][0] + avg_distance * cos(angle_trunc(heading1+turning))
                est_y = OTHER['measurements'][-1][1] + avg_distance * sin(angle_trunc(heading1+turning))
        else:
            #print 'bye'
            line_length = mode_length(OTHER['line_length'])
            if line_length == 1:
                #print '0'
                avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
                heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
                heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
                turning = abs(heading1 - heading2)
                est_heading = angle_trunc(turning + heading1)
                est_x = OTHER['measurements'][-1][0] + avg_distance * cos(angle_trunc(heading1+turning))
                est_y = OTHER['measurements'][-1][1] + avg_distance * sin(angle_trunc(heading1+turning))
            elif line_length == 0 or len(OTHER['measurements']) % line_length == 0:
                if OTHER['EKF_status']  == 0:
                    #print 'running EKF'
                    avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
                    heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
                    heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
                    #turning = OTHER['turning']
                    turning = heading1 - heading2
                    est_heading = angle_trunc(turning + heading1)
                    x = matrix([[OTHER['measurements'][-1][0]], [OTHER['measurements'][-1][1]], [est_heading], [turning], [avg_distance]])
                    OTHER['EKF_x'] = x
                    OTHER['EKF_status'] = 1
                else:
                    #print 'running EKF'
                    avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
                    heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
                    heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
                    #turning = OTHER['turning']
                    turning = heading1 - heading2
                    est_heading = angle_trunc(turning + heading1)
                    x = matrix([[OTHER['xy_estimate'][0]], [OTHER['xy_estimate'][1]], [est_heading], [turning], [avg_distance]])
                    OTHER['EKF_x'] = x
                    
                
                (est_x, est_y), OTHER = EKF(OTHER, measurement, 0.05*avg_distance)
            else:
                #print '2'
                est_x = OTHER['measurements'][-1][0] + avg_distance * cos(heading1)
                est_y = OTHER['measurements'][-1][1] + avg_distance * sin(heading1)
#        elif OTHER['line_length_calculated'] == 1:
#            line_length = OTHER['line_length']
    
    xy_estimate = (est_x, est_y)
    OTHER['xy_estimate'] = xy_estimate
    #print("Estimate of Iteration " +str(len(OTHER)) + "estimated value = " + str(xy_estimate))
    return xy_estimate, OTHER


def EKF (OTHER, measurement, measurement_noise =0):
        x = OTHER['EKF_x']
        P = OTHER['EKF_P']
        heading = x.value[2]
        turn = x.value[3]
        distance1 = x.value[4]
    
        H = matrix([[1,0,0,0,0],[0,1,0,0,0]])
        R = matrix([[measurement_noise,0],[0,measurement_noise]])
        I = matrix([[1,0,0,0,0],[0,1,0,0,0],[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]]) 
        u = matrix([[0.], [0.], [0.], [0.], [0.]])
        # measurement update
        Z = matrix([[measurement[0]],[measurement[1]]])
        y = Z - (H * x)
        S = H * P * H.transpose() + R
        K = P * H.transpose() * S.inverse()
        x = x + (K * y)
        #print x
        P = (I - (K * H)) * P
           
        # prediction
        distance1 = x.value[4][0]
        heading = x.value[2][0]
        turn = x.value[3][0]
        #x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        
        dx_h = -distance1 * sin(heading + turn) 
        dx_t = -distance1 * sin(heading + turn)
        dx_d = cos(heading + turn)
        dy_h = distance1 * cos(heading + turn)
        dy_t = distance1 * cos(heading + turn)
        dy_d = sin(heading + turn) 
        F = matrix([[1,0,dx_h,dx_t,dx_d],[0,1,dy_h,dy_t,dy_d],[0,0,1,1,0],[0,0,0,1,0],[0,0,0,0,1]])
        
        P = F * P * F.transpose()
        OTHER['EKF_x'] = x_dash
        OTHER['EKF_P'] = P
        xy_estimate = (x_dash.value[0][0],x_dash.value[1][0])
        return xy_estimate, OTHER
# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)