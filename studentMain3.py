from math import *
from matrix import *
from robot import *
import random

    # This function will be called after each time the target moves. 
    # The OTHER variable is a place for you to store any historical 
    # information about the progress of the hunt (or maybe some 
    # localization information). Your must return a tuple of three 
    # values: turning, distance, OTHER

def filter(x, P, measurements, F, H, I, R):
    for n in range(len(measurements)):
        
        # measurement update
        z = matrix([[measurement[0]], [measurement[1]]])
        #y = Z.transpose() - (H * x)
        y = Z - (H * x)
        S = H * P * H.transpose() + R
        K = P * H.transpose() * S.inverse()
        x = x + (K * y)
        P = (I - (K * H)) * P
        
        # prediction
        x = (F * x) + u
        P = F * P * F.transpose()
        
        
    
    print 'x= '
    x.show()
    print 'P= '
    P.show()
    
    return x, P
def mode_length(line_len):
    return max(set(line_len), key=line_len.count)

def get_heading(hunter_position, target_position):
    
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading


def next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER = None):
    #Change these:
    if OTHER is None:
        heading = 0
        turn = 0
        distance1 = 0
        x = matrix([[target_measurement[0]], [target_measurement[1]], [heading], [turn], [distance1]])
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        OTHER = {'steps': 0, 'EKF_status' : 0,'measurements': [], 'EKF_x': x, 'EKF_P': P, 'line_length' : [], 'line_length_calculated' : 0, 'xy_estimate' : (0,0), 'turning' : 0, 'heading': 0, 'distance' : []}
    
    OTHER['measurements'].append(target_measurement)
    
    if len(OTHER['measurements']) == 1:
        OTHER['steps'] = OTHER['steps'] + 1
        est_x = OTHER['measurements'][-1][0]
        est_y = OTHER['measurements'][-1][1]
        xy_estimate = OTHER['measurements'][-1][0], OTHER['measurements'][-1][1]
    
    elif len(OTHER['measurements']) == 2:
        OTHER['steps'] = OTHER['steps'] + 1
        est_x = OTHER['measurements'][-1][0]
        est_y = OTHER['measurements'][-1][1]
        xy_estimate = OTHER['measurements'][-1][0], OTHER['measurements'][-1][1]
    
    elif len(OTHER['measurements']) == 3:
        OTHER['steps'] = OTHER['steps'] + 1
        distance1 = distance_between(OTHER['measurements'][-1], OTHER['measurements'][-2])
        heading = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
        est_x = OTHER['measurements'][-1][0] + distance1 * cos(heading)
        est_y = OTHER['measurements'][-1][1] + distance1 * sin(heading)
    else:
        distance1 = distance_between(OTHER['measurements'][-1], OTHER['measurements'][-2])
        OTHER['distance'].append((distance1))
        avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
        heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
        heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
        turning = abs(heading1 - heading2)
        OTHER['heading'] = angle_trunc(turning + heading1)
        if OTHER['line_length_calculated'] == 0 :
            #OTHER['steps'] = OTHER['steps'] + 1
            if turning < 2*pi/14:
                est_x = OTHER['measurements'][-1][0] + distance1 * cos(heading1)
                est_y = OTHER['measurements'][-1][1] + distance1 * sin(heading1)
                OTHER['steps'] = OTHER['steps'] + 1
            
            else:
                OTHER['turning'] = turning
                if len(OTHER['line_length']) > 0:
                    line_len = mode_length(OTHER['line_length'])
                else:
                    line_len = 0
                if line_len == OTHER['steps']:
                    OTHER['line_length_calculated'] = 1
                    
                
                OTHER['line_length'].append(OTHER['steps'])
                OTHER['steps'] = 1  
                est_x = OTHER['measurements'][-1][0] + avg_distance * cos(angle_trunc(heading1+turning))
                est_y = OTHER['measurements'][-1][1] + avg_distance * sin(angle_trunc(heading1+turning))
        else:
            #print 'hi'
            line_length = mode_length(OTHER['line_length'])
            if line_length == 1:
                #print '0'
                avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
                heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
                heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
                turning = abs(heading1 - heading2)
                est_heading = angle_trunc(turning + heading1)
                est_x = OTHER['measurements'][-1][0] + avg_distance * cos(angle_trunc(heading1+turning))
                est_y = OTHER['measurements'][-1][1] + avg_distance * sin(angle_trunc(heading1+turning))
            elif line_length == 0 or len(OTHER['measurements']) % line_length == 0:
                if OTHER['EKF_status']  == 0:
                    #print 'running EKF'
                    avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
                    heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
                    heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
                    #turning = OTHER['turning']
                    turning = heading1 - heading2
                    est_heading = angle_trunc(turning + heading1)
                    x = matrix([[OTHER['measurements'][-1][0]], [OTHER['measurements'][-1][1]], [est_heading], [turning], [avg_distance]])
                    OTHER['EKF_x'] = x
                    OTHER['EKF_status'] = 1
                else:
                    #print 'running EKF'
                    avg_distance = sum(OTHER['distance'])/len(OTHER['distance'])
                    heading1 = atan2(OTHER['measurements'][-1][1] - OTHER['measurements'][-2][1], OTHER['measurements'][-1][0] - OTHER['measurements'][-2][0])
                    heading2 = atan2(OTHER['measurements'][-2][1] - OTHER['measurements'][-3][1], OTHER['measurements'][-2][0] - OTHER['measurements'][-3][0])
                    #turning = OTHER['turning']
                    turning = heading1 - heading2
                    est_heading = angle_trunc(turning + heading1)
                    x = matrix([[OTHER['xy_estimate'][0]], [OTHER['xy_estimate'][1]], [est_heading], [turning], [avg_distance]])
                    OTHER['EKF_x'] = x
                    
                
                (est_x, est_y), OTHER = EKF(OTHER, target_measurement, 0.05*avg_distance)
                xy_estimate = (est_x, est_y)
            else:
                #print '2'
                est_x = OTHER['measurements'][-1][0] + avg_distance * cos(heading1)
                est_y = OTHER['measurements'][-1][1] + avg_distance * sin(heading1)
#        elif OTHER['line_length_calculated'] == 1:
#            line_length = OTHER['line_length']
    
    xy_estimate = (est_x, est_y)
    OTHER['xy_estimate'] = xy_estimate
    #print("Estimate of Iteration " +str(len(OTHER)) + "estimated value = " + str(xy_estimate))
    target_distance = distance_between(hunter_position, xy_estimate)
    target_heading = get_heading(hunter_position, (est_x,est_y))
    hunter_turning = target_heading - hunter_heading
    hunter_turning = angle_trunc(hunter_turning)

    if target_distance > max_distance:
        target_distance = max_distance

    return hunter_turning, target_distance, OTHER

def EKF (OTHER, measurement, measurement_noise =0):
        x = OTHER['EKF_x']
        P = OTHER['EKF_P']
        heading = x.value[2]
        turn = x.value[3]
        distance1 = x.value[4]
    
        H = matrix([[1,0,0,0,0],[0,1,0,0,0]])
        R = matrix([[measurement_noise,0],[0,measurement_noise]])
        I = matrix([[1,0,0,0,0],[0,1,0,0,0],[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]]) 
        u = matrix([[0.], [0.], [0.], [0.], [0.]])
        # measurement update
        Z = matrix([[measurement[0]],[measurement[1]]])
        y = Z - (H * x)
        S = H * P * H.transpose() + R
        K = P * H.transpose() * S.inverse()
        x = x + (K * y)
        #print x
        P = (I - (K * H)) * P
           
        # prediction
        distance1 = x.value[4][0]
        heading = x.value[2][0]
        turn = x.value[3][0]
        #x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        
        dx_h = -distance1 * sin(heading + turn) 
        dx_t = -distance1 * sin(heading + turn)
        dx_d = cos(heading + turn)
        dy_h = distance1 * cos(heading + turn)
        dy_t = distance1 * cos(heading + turn)
        dy_d = sin(heading + turn) 
        F = matrix([[1,0,dx_h,dx_t,dx_d],[0,1,dy_h,dy_t,dy_d],[0,0,1,1,0],[0,0,0,1,0],[0,0,0,0,1]])
        
        P = F * P * F.transpose()
        OTHER['EKF_x'] = x_dash
        OTHER['EKF_P'] = P
        xy_estimate = (x_dash.value[0][0],x_dash.value[1][0])
        return xy_estimate, OTHER
# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
